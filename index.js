const express = require('express'); //use express
const app = express(); //object to use express as app
const bodyParser = require('body-parser');
const sqlite3 = require('sqlite3').verbose(); //use of sqlite3 install with npm sqlite3
let db = new sqlite3.Database('./db/db.sqlite');  //create databe with name: db

let usersController = require('./app/controllers/users')(db); //create variable to indicate where search users
let classesController = require('./app/controllers/classes')(db);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true})); //configuration to accept elements of body
app.use('/users',usersController);
app.use('/classes',classesController);

app.listen(3310,function(){
    console.log("Running");
});